<?php


namespace App\Transformer;


use App\Models\BsdImage;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

class BsdImageTransformer extends TransformerAbstract
{

    public function transform(BsdImage $bsdImage)
    {

        try {
            $createdDate = Carbon::parse($bsdImage->created_at)->format('d M Y');
            $updatedDate = Carbon::parse($bsdImage->updated_at)->format('d M Y');

            $itemEditUrl = route('admin.images.edit', ['item' => $bsdImage->id]);

//            $action = "<a class='btn btn-xs btn-primary' href='".$itemShowUrl."' data-toggle='tooltip' data-placement='top'><i class='fa fa-info'></i></a> ";
            $action = "<a class='btn btn-xs btn-info' href='" . $itemEditUrl . "' data-toggle='tooltip' data-placement='top'><i class='fa fa-edit'></i></a> ";

            return [
                'section_name'          => $bsdImage->category->name,
                'section_sub_name'      => $bsdImage->name,
                'image_path'            => $bsdImage->location,
                'created_at'            => $createdDate,
                'update_at'             => $updatedDate,
                'action'                => $action
            ];
        } catch (\Exception $exception) {
            error_log($exception);
        }
    }

}

<?php


namespace App\Http\Controllers\Admin;


class ImageConstants
{
    public static $IMAGE_PATH = "images/bsdcustomer/";
    public static $IMAGE_PATH_HOME = "images/bsdcustomer/home";
    public static $IMAGE_PATH_PROJECT = "images/bsdcustomer/project";
    public static $IMAGE_PATH_DIGITAL = "images/bsdcustomer/digital";
    public static $IMAGE_PATH_SOP = "images/bsdcustomer/sop";

    // Categories
    public static $CATEGORY_CUSTOMER_JOURNEY = 'Customer Journey';
    public static $CATEGORY_PROJECT_DESCRIPTION = "Project Description";
    public static $CATEGORY_DIGITAL_HARDWARE = "Digital Hardware";
    public static $CATEGORY_SOP = "Sales SOP & Outfit Guide";
    // Sub Categories
    public static $CATEGORY_SUB_CUSTOMER_JOURNEY = ["Regular Customer", "VIP Customer"];
    public static $CATEGORY_SUB_REGULAR_CUSTOMER = "Regular Customer";
    public static $CUSTOMER_SUB_VIP_CUSTOMER = "VIP Customer";

    public static $CATEGORY_SUB_PROJECT_DESCRIPTION = ["Apartment", "Business Loft", "Industrial Lot",
        "Landed House", "Office", "Shophouse", "Soho", "Other"];
    public static $CUSTOMER_SUB_APARTMENT = "Apartment";
    public static $CUSTOMER_SUB_BUSINESS_LOFT = "Business Loft";
    public static $CUSTOMER_SUB_INDUSTRIAL_LOT = "Industrial Lot";
    public static $CUSTOMER_SUB_LANDED_HOUSE = "Landed House";
    public static $CUSTOMER_SUB_OFFICE = "Office";
    public static $CUSTOMER_SUB_SHOPHOUSE = "Shophouse";
    public static $CUSTOMER_SUB_SOHO = "Soho";
    public static $CUSTOMER_SUB_OTHER = "Other";

    public static $CATEGORY_SUB_DIGITAL_HARDWARE = ["AR Kids Coloring"];
    public static $CUSTOMER_SUB_AR_KIDS_COLORING = "AR Kids Coloring";

    public static $CATEGORY_SUB_SOP = ["Absensi Kehadiran", "Tata Cara Berpakaian", "Operasional Kerja", "Penetapan Sanksi"];
    public static $CUSTOMER_SUB_ATTENDANCE = "Absensi Kehadiran";
    public static $CUSTOMER_SUB_OUTFIT_GUIDE = "Tata Cara Berpakaian";
    public static $CUSTOMER_SUB_WORK_SOP = "Operasional Kerja";
    public static $CUSTOMER_SUB_PENALTY = "Penetapan Sanksi";
}

<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\BsdImage;
use App\Models\Category;
use App\Models\PortofolioImage;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Intervention\Image\Facades\Image;

class ImageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function indexDefault(Request $request)
    {
        return redirect()->route('admin.images.index', ['category' => ImageConstants::$CATEGORY_CUSTOMER_JOURNEY]);
    }

    public function index(Request $request)
    {
        return view('admin.images.index')/*->with('category', $request->category)*/ ;
    }

    public function show(BsdImage $item)
    {
        $images = PortofolioImage::where('portofolio_id', $item->id)->orderby('is_main_image', 'desc')->get();
        $productCategory = Category::where('portofolio_id', $item->id)->first();

        $data = [
            'product' => $item,
            'productCategory' => $productCategory,
            'images' => $images,
        ];
        return view('admin.portofolio.show')->with($data);
    }


    public function update(Request $request)
    {
        try {
            if (!$request->hasFile('main_image')) {
                /*Log::info('Image and stuffs '
                    .$request->input('id')
                    .$request->input('category_name')
                    .$request->input('category_sub_name')
                    .$request->input('image_base_path')
                    .$request->input('image_path'));*/
                return back()->withErrors("Gambar Utama wajib diunggah!")->withInput($request->all());
            }

            $mainImage = $request->file('main_image');

            $dateTimeNow = Carbon::now('Asia/Jakarta');

            $imageId = $request->input('id');
            $category_name = $request->input('category_name');
            $category_sub_name = $request->input('category_sub_name');
            $imageBasePath = $request->input('image_base_path');
            $imagePath = $request->input('image_path');

            $oldImageFromDb = BsdImage::where('image_base_path', $imageBasePath)->where('image_path', $imagePath)->first();

            if ($oldImageFromDb->created_at == null) {
                $oldImageFromDb->created_at = $dateTimeNow->toDateTimeString();
                $oldImageFromDb->updated_at = $dateTimeNow->toDateTimeString();
                $oldImageFromDb->save();
            }

            $newImage = ([
                'category_name' => $category_name,
                'category_sub_name' => $category_sub_name,
                'image_base_path' => $imageBasePath,
                'image_path' => $imagePath,
                'created_at' => $dateTimeNow->toDateTimeString(),
                'updated_at' => $dateTimeNow->toDateTimeString(),
            ]);

            //main image
            $img = Image::make($mainImage);
            $extStr = $img->mime();
            $ext = explode('/', $extStr, 2);
//            $filename = $newImage->id.'_main_'.Carbon::now('Asia/Jakarta')->format('Ymdhms'). '.'. $ext[1];
//            $filename = $request->input('file_name'). '.'. $ext[1];

            $img->save(public_path('images/bsdcustomer/' . $imageBasePath.'/'.$imagePath), 75);

            $matchThese = [
                'id'        =>$imageId
            ];

            BsdImage::updateOrCreate($matchThese,[
                'updated_at'=>$dateTimeNow->toDateTimeString()
            ]);

            Session::flash('success', 'Sukses mengupdate gambar!');
            return redirect()->route('admin.images.edit', ['category' => $category_name, 'id' => $imageId]);

        } catch (\Exception $ex) {
            if (!empty($img)) {
                $img->destroy();
            }
            error_log($ex);
            Log::error("Admin/ImageController store error: " . $ex);
            return back()->withErrors("Something Went Wrong")->withInput();
        }
    }

    public function edit(string $category, int $id)
    {
        try {
            /*$categoryName = $item->category_name;
            $categorySubName = $item->category_name;
            $mainImage = PortofolioImage::where('portofolio_id', $item->id)->where('is_main_image', 1)->first();
            $detailImage = PortofolioImage::where('portofolio_id', $item->id)
                ->where('is_main_image', 0)
                ->where('is_thumbnail', 0)->get();
            $data = [
                'image'    => $item,
                'categories'    => $categories,
                'mainImage'    => $mainImage,
                'detailImage'    => $detailImage,
            ];*/
            $BsdImage = BsdImage::where('id', $id)->first();
            $data = [
                'image' => $BsdImage
            ];
            return view('admin.images.edit')->with($data);

        } catch (\Exception $ex) {
            Log::error("Admin/PortofolioController edit error: " . $ex);
            dd($ex);
        }
    }
}

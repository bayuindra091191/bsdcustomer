<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 17 May 2020 17:20:00 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class BsdImage
 *
 * @property int $id
 * @property string $section_name
 * @property string $section_sub_name
 * @property string $image_path
 * @property \Carbon\Carbon $created_at
 * @property int $created_by
 * @property \Carbon\Carbon $updated_at
 * @property int $updated_by
 *
 * @package App\Models
 */
class BsdImage extends Eloquent
{
	protected $casts = [
        'id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int'
	];

	protected $fillable = [
		'category_name',
		'category_sub_name',
        'image_base_path',
		'image_path',
		'created_by',
		'updated_by'
	];
}

@extends('layouts.admin')

@section('content')

    <header class="blue accent-3 relative nav-sticky">
        <div class="container-fluid text-white">
            <div class="row p-t-b-10 ">
                <div class="col">
                    <h4><i class="fa fa-table"></i> {{request()->category}}</h4>
                </div>
            </div>
        </div>
    </header>

    <div class="content-wrapper animatedParent animateOnce">
        <div class="container">
            <section class="paper-card">
                <div class="row">
                    <div class="col-12">
                        @include('partials.admin._messages')
                        {{--<table id="demoGrid" class="table table-striped table-bordered dt-responsive nowrap" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>Sub-Kategori</th>
                                <th>Gambar</th>
                                <th>Dibuat pada</th>
                                <th>Diubah pada</th>
                                <th>Tindakan</th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>--}}

                        <ul>

                            @foreach($images = \App\Models\BsdImage::where('category_name', request()->category)->get() as $image)
                                <li class="image-container">
                                    <img
                                        src="{{ asset(\App\Http\Controllers\Admin\ImageConstants::$IMAGE_PATH.$image->image_base_path.'/'.$image->image_path) }}"
                                        style="width: 200px;height: auto;">

                                    {{--@if(request()->category === ImageConstants::$CATEGORY_CUSTOMER_JOURNEY)
                                        <img src="{{ asset(ImageConstants::$IMAGE_PATH
                                        .'home/'
                                        .$image->image_path) }}" style="width: 200px;height: auto;">
                                    @elseif(request()->category === ImageConstants::$CATEGORY_PROJECT_DESCRIPTION)
                                        <img src="{{ asset(ImageConstants::$IMAGE_PATH
                                        .'project/'
                                        .$image->image_path) }}" style="width: 200px;height: auto;">
                                    @elseif(request()->category === ImageConstants::$CATEGORY_DIGITAL_HARDWARE)
                                        <img src="{{ asset(ImageConstants::$IMAGE_PATH
                                        .'digital/'
                                        .$image->image_path) }}" style="width: 200px;height: auto;">
                                    @elseif(request()->category === ImageConstants::$CATEGORY_SOP)
                                        <img src="{{ asset(ImageConstants::$IMAGE_PATH
                                        .'sop/'
                                        .$image->image_path) }}" style="width: 200px;height: auto;">
                                    @endIf--}}

                                    <div class="image-description-wrapper">
                                        <h2>{{$image->category_sub_name}}</h2>

                                        <a class="image-description-wrapper-edit"
                                           href="{{ route('admin.images.edit', ['category' => $image->category_name, 'id' => $image->id]) }}">
                                            Edit
                                        </a>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </section>
        </div>
    </div>

    <div id="deleteModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>
                <div class="modal-body">
                    <h3 class="text-center">Apakah anda yakin ingin menghapus data ini?</h3>
                    <br/>

                    <form role="form">
                        <input type="hidden" id="deleted-id" name="deleted-id"/>
                    </form>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-warning" data-dismiss="modal">
                            <span class='glyphicon glyphicon-remove'></span> No
                        </button>
                        <button type="submit" class="btn btn-danger delete">
                            <span class='glyphicon glyphicon-trash'></span> Yes
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">

    <style type="text/css">
        .image-container {
            margin-top: 10px;
            padding: 10px;
            width: 75%;
            position: relative;
            border-bottom: 1px solid #cfcfcf;
        }

        .image-container:last-child {
            border-bottom: 0;
        }

        .image-description-wrapper {
            margin-left: 20px;
            display: inline-block;
            vertical-align: top;
            position: absolute;
            top: 25%;
        }

        .image-description-wrapper-edit {

        }

    </style>

@endsection

@section('scripts')
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
@endsection

@extends('layouts.admin')

@section('content')

    <header class="blue accent-3 relative nav-sticky">
        <div class="container-fluid text-white">
            <div class="row p-t-b-10 ">
                <div class="col">
                    <h4><i class="icon-table"></i> Ubah Gambar</h4>
                </div>
            </div>
        </div>
    </header>

    <div class="content-wrapper animatedParent animateOnce">
        <div class="container">
            <section class="paper-card">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body b-b">

                                <div class="container-fluid animatedParent animateOnce my-3">
                                    <div class="animated fadeInUpShort">
                                        <!-- Input -->
                                        {{ Form::open(['route'=>['admin.images.update'],'method' => 'post','id' => 'general-form', 'enctype' => 'multipart/form-data']) }}

                                        @include('partials.admin._messages')
                                        @foreach($errors->all() as $error)
                                            <ul>
                                                <li>
                                            <span class="help-block">
                                                <strong style="color: #ff3d00;"> {{ $error }} </strong>
                                            </span>
                                                </li>
                                            </ul>
                                        @endforeach
                                        <div class="row">
                                            <label class="form-label"><h2>{{$image->category_sub_name}}</h2></label>
                                            <div class="col-md-12" style="margin-top: 20px">
                                                <div class="row">
                                                    <div class="col-md-6 mb-3">
                                                        <div style="float: left">
                                                            <input type="hidden" name="id" value="{{ $image->id }}">
                                                            <input type="hidden" name="category_name" value="{{ $image->category_name }}">
                                                            <input type="hidden" name="category_sub_name" value="{{ $image->category_sub_name }}">
                                                            <input type="hidden" name="image_base_path" value="{{ $image->image_base_path }}">
                                                            <input type="hidden" name="image_path" value="{{ $image->image_path }}">
                                                            @if(!empty($image))
                                                                {{--@if($image->category_name === ImageConstants::$CATEGORY_CUSTOMER_JOURNEY)
                                                                    <img src="{{ asset(ImageConstants::$IMAGE_PATH_HOME.$image->image_path) }}" style="width: 200px;height: auto;">
                                                                @elseif($image->category_name === ImageConstants::$CATEGORY_PROJECT_DESCRIPTION)
                                                                    <img src="{{ asset(ImageConstants::$IMAGE_PATH_PROJECT.$image->image_path) }}" style="width: 200px;height: auto;">
                                                                @elseif($image->category_name === ImageConstants::$CATEGORY_DIGITAL_HARDWARE)
                                                                    <img src="{{ asset(ImageConstants::$IMAGE_PATH_DIGITAL.$image->image_path) }}" style="width: 200px;height: auto;">
                                                                @elseif($image->category_name === ImageConstants::$CATEGORY_SOP)
                                                                    <img src="{{ asset(ImageConstants::$IMAGE_PATH_SOP.$image->image_path) }}" style="width: 200px;height: auto;">
                                                                @endIf--}}

                                                                <img
                                                                    src="{{ asset(ImageConstants::$IMAGE_PATH.$image->image_base_path.'/'.$image->image_path) }}"
                                                                    style="width: 200px;height: auto;">
                                                            @endif
                                                        </div>

                                                    </div>
                                                    <div class="col-md-6 mb-3">
                                                        {!! Form::file('main_image', array('id' => 'main_image', 'name'=>"main_image", 'class' => 'file-loading', 'accept' => 'image/*')) !!}
                                                    </div>
                                                </div>
                                                <div class="row" style="margin-top: 20px">
                                                    <a href="{{ route('admin.images.index', ['category' => $image->category_name]) }}"
                                                       class="btn btn-warning">Batal</a>
                                                    <button class="btn btn-success" type="submit" style="margin-left: 12px">Simpan</button>
                                                </div>
                                            </div>
                                        </div>
                                    {{ Form::close() }}
                                    <!-- #END# Input -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection


@section('styles')
    <link href="{{ URL::asset('css/fileinput.css') }}" rel="stylesheet">
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ URL::asset('js/fileinput.js') }}"></script>

    <script>

        // FILEINPUT
        $("#main_image")
            .fileinput({
                allowedFileExtensions: ["jpg", "jpeg", "png"],
                showUpload: false,
            });
    </script>
@endsection

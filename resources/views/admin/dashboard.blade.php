@extends('layouts.admin')

@section('content')

    <header class="blue accent-3 relative nav-sticky">
        <div class="container-fluid text-white">
            <div class="row p-t-b-10 ">
                <div class="col">
                    <h4> <i class="fa fa-table"></i> Dashboard</h4>
                </div>
            </div>
        </div>
    </header>

    <div class="content-wrapper animatedParent animateOnce">
        <div class="container-fluid">
            <section class="paper-card">
                <div class="row">
                    <div class="col-12">
                        <h3>Selamat Datang Admin BSD Customer Journey</h3>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection

@section('styles')
@endsection

@section('scripts')
    </script>
@endsection

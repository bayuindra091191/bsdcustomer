@extends('layouts.frontend')

@section('head_and_title')
    <meta name="description" content="Escalier Portfolio">
    <meta name="author" content="PT. Generasi Muda Gigih">
    <meta name="keywords" content="Escalier, Interior, Office, Residential, Apartment, Commercial, Spaces">

    <title>SALES SOP & OUTFIT GUIDE</title>
@endsection

@section('content')

    <section class="bg-white pt-5 section-body">
        <div class="container-no-padding no-gutters">
            <!-- Title section -->
            <div class="row-nav-tab">
                {{--<div class="col-md-3 col-1"></div>--}}
                    <!-- Tab02 -->
                    <div class="text-center swiper-container">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs swiper-wrapper" role="tablist">
                            {{-- Absensi & Kehadiran --}}
                            <li class="nav-item tab-nav swiper-slide">
                                <a class="nav-link active text-dark font-montserrat-medium nav-custom-box " data-toggle="tab" href="#absensi" role="tab">
                                    <div>
                                        ABSENSI & KEHADIRAN
                                    </div>
                                    {{--<div class="d-block d-md-none">
                                        ABSENSI & <br/>
                                        KEHADIRAN
                                    </div>--}}
                                </a>
                            </li>
                            {{-- Tata Cara Berpakaian --}}
                            <li class="nav-item tab-nav swiper-slide">
                                <a class="nav-link text-dark font-montserrat nav-custom-box font-weight-bold" data-toggle="tab" href="#tata-berpakaian" role="tab">
                                    <div {{--class="d-none d-md-block"--}}>
                                        TATA CARA BERPAKAIAN
                                    </div>
                                    {{--<div class="d-block d-md-none">
                                        TATA CARA<br/>
                                        BERPAKAIAN
                                    </div>--}}
                                </a>
                            </li>
                            {{-- Operasional Kerja --}}
                            <li class="nav-item tab-nav swiper-slide">
                                <a class="nav-link text-dark font-montserrat nav-custom-box font-weight-bold" data-toggle="tab" href="#operasional" role="tab">
                                    <div {{--class="d-none d-md-block"--}}>
                                        OPERASIONAL KERJA
                                    </div>
                                    {{--<div class="d-block d-md-none">
                                        OPERASIONAL<br/>
                                        KERJA
                                    </div>--}}
                                </a>
                            </li>
                            {{-- Penetapan Sanksi --}}
                            <li class="nav-item tab-nav swiper-slide">
                                <a class="nav-link text-dark font-montserrat nav-custom-box font-weight-bold" data-toggle="tab" href="#sanksi" role="tab">
                                    <div {{--class="d-none d-md-block"--}}>
                                        PENETAPAN SANKSI
                                    </div>
                                    {{--<div class="d-block d-md-none">
                                        PENETAPAN<br/>
                                        SANKSI
                                    </div>--}}
                                </a>
                            </li>
                        </ul>
                    </div>
                {{--<div class="col-md-3 col-1"></div>--}}
            </div>
        </div>
    </section>

    <section class="bg-white">
        <div class="tab-content pb-5">
            {{-- Absensi --}}
            <div class="tab-pane fade show active" id="absensi" role="tabpanel">
                <div class="container-fluid">
                    <div class="row no-gutters">
                        <div class="col-12 w-100">
                            <img class="img-content" src="{{ asset('images/bsdcustomer/sop/1-absensi-kehadiran.jpg') }}"/>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Tata Cara Berpakaian--}}
            <div class="tab-pane fade show" id="tata-berpakaian" role="tabpanel">
                <div class="container-fluid">
                    <div class="row no-gutters">
                        <div class="col-12 w-100 {{--bg-tata-berpakaian--}}">
                            <img class="img-content" src="{{ asset('images/bsdcustomer/sop/2-tata-cara-berpakaian.jpg') }}"/>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Operasional --}}
            <div class="tab-pane fade show" id="operasional" role="tabpanel">
                <div class="container-fluid">
                    <div class="row no-gutters">
                        <div class="col-12 w-100{{-- bg-operasional--}}">
                            <img class="img-content" src="{{ asset('images/bsdcustomer/sop/3-operasional-kerja.jpg') }}"/>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Sanksi --}}
            <div class="tab-pane fade show" id="sanksi" role="tabpanel">
                <div class="container-fluid">
                    <div class="row no-gutters">
                        <div class="col-12 w-100{{-- bg-sanksi--}}">
                            <img class="img-content" src="{{ asset('images/bsdcustomer/sop/4-penetapan-sanksi.jpg') }}"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="https://unpkg.com/swiper/css/swiper.css">


    <style type="text/css">

        .bg-regular{
            background-image: url('{{ asset('images/bsdcustomer/home/customer journey - regular.jpg') }}');
            background-repeat: no-repeat;
            background-position: center;
            background-size: contain;
            height:1100px;">
        }

        .bg-vip{
            background-image: url('{{ asset('images/bsdcustomer/home/customer journey - vip.jpg') }}');
            background-repeat: no-repeat;
            background-position: center;
            background-size: contain;
            height:830px;">
        }

        .img-content{
            width: 100%;
            object-fit: contain
        }

        .bg-absensi{
            background-image: url('{{ asset('images/bsdcustomer/sop/1-absensi-kehadiran.jpg') }}');
            background-repeat: no-repeat;
            background-position: center;
            background-size: contain;
            width:100%;">
        }

        .bg-tata-berpakaian{
            background-image: url('{{ asset('images/bsdcustomer/sop/2-tata-cara-berpakaian.jpg') }}');
            background-repeat: no-repeat;
            background-position: center;
            background-size: contain;
            height:830px;">
        }

        .bg-operasional{
            background-image: url('{{ asset('images/bsdcustomer/sop/3-operasional-kerja.jpg') }}');
            background-repeat: no-repeat;
            background-position: center;
            background-size: contain;
            height:830px;">
        }

        .bg-sanksi{
            background-image: url('{{ asset('images/bsdcustomer/sop/4-penetapan-sanksi.jpg') }}');
            background-repeat: no-repeat;
            background-position: center;
            background-size: contain;
            height:830px;">
        }

        .container-fluid{
            padding:0px;
        }
        .header-img{
            height: 190px;
        }

        .box-portofolio{
            margin: 0 auto;
            width: 290px;
            height: 360px;
        }

        .image-portofolio{
            height: 300px;
            background-repeat: no-repeat;
            background-position: center;
            background-size: cover;
            border-radius: 7px;
        }

        .spaces-portfolio-responsive {
            padding-top: 1em;
        }

        .tab-content>.tab-pane,

        .pill-content>.pill-pane {

            display: block;
            /* undo display:none          */

            height: 0;
            /* height:0 is also invisible */

            overflow-y: hidden;
            /* no-overflow                */

        }

        .tab-content>.active,

        .pill-content>.active {

            height: auto;
            /* let the content decide it  */

            overflow-y: visible;

        }

        .header-portfolio-text{
            font-size: 20px;
        }

        .tab-nav {
            border: none !important;
        }

        @media (min-width: 350px) {
            .box-portofolio{
                width: 345px;
                height: 430px;
            }

            .image-portofolio{
                height: 370px;
                background-repeat: no-repeat;
                background-position: center;
                background-size: cover;
                border-radius: 7px;
            }
        }

        @media (min-width: 400px) {
            .box-portofolio{
                width: 385px;
                height: 430px;
            }

            .image-portofolio{
                height: 370px;
                background-repeat: no-repeat;
                background-position: center;
                background-size: cover;
                border-radius: 7px;
            }
        }

        @media (min-width: 768px) {
            .bg-vip{
                height:2800px;
            }
            .bg-regular{
                height: 3700px;
            }
            .header-img{
                height: 500px;
            }

            .box-portofolio{
                width: 370px;
                height: 430px;
            }

            .image-portofolio{
                height: 370px;
                background-repeat: no-repeat;
                background-position: center;
                background-size: cover;
                border-radius: 7px;
            }

            .header-portfolio-text{
                font-size: 35px;
            }
        }

        @media (min-width: 992px) {}

        @media (min-width: 1024px) {

            .left-menu {
                border-right: 1px solid #333;
                border-top: 1px solid #333;
            }

            .middle-menu {
                border-top: 1px solid #333;
                border-bottom: 1px solid #333;
            }

            .right-menu {
                border-left: 1px solid #333;
                border-top: 1px solid #333;
                border-bottom: 1px solid #333;
            }

            .tab-left {
                border-top: 1px solid #333;
                border-bottom: 1px solid #333;
                border-right: 1px solid #333;
            }

            .tab-right {
                border-top: 1px solid #333;
                border-bottom: 1px solid #333;
            }

            .four-box {
                height: 100px;
                text-align: center;
                vertical-align: middle;
                line-height: 90px;
            }

            /*.tab-nav {*/
            /*    border-top: 1px solid #333;*/
            /*    border-left: 1px solid #333;*/
            /*    border-bottom: 1px solid #333;*/
            /*}*/

            /*.tab-nav:last-child{*/
            /*    border-right: 1px solid #333;*/
            /*}*/
        }

        @media (min-width: 1200px) {
            .left-menu {
                border-left: 1px solid #333;
                border-right: 1px solid #333;
                border-top: 1px solid #333;
            }

            .right-menu {
                border-right: 1px solid #333;
                border-top: 1px solid #333;
                border-bottom: 1px solid #333;
            }

            .four-box {
                text-align: center;
                vertical-align: middle;
                line-height: 90px;
            }
        }
    </style>
@endsection

@section('scripts')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.min.js"></script>

    <script src="https://unpkg.com/swiper/js/swiper.js"></script>

    <script>
        var swiper = new Swiper('.swiper-container', {
            slidesPerView: 'auto',
            spaceBetween: 20,
            freeMode: true,
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            },
        });
    </script>
@endsection

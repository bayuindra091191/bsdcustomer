@extends('layouts.frontend')

@section('head_and_title')
    <meta name="description" content="Escalier Portfolio">
    <meta name="author" content="PT. Generasi Muda Gigih">
    <meta name="keywords" content="Escalier, Interior, Office, Residential, Apartment, Commercial, Spaces">

    <title>DIGITAL HARDWARE MANUAL</title>
@endsection

@section('content')

    <section class="bg-white pt-5 pb-3 section-body">
        <div class="container">
            <!-- Title section -->
            <div class="row">
                <div class="col-md-4 col-2"></div>
                <div class="col-md-4 col-8">
                    <!-- Tab02 -->
                    <div class="text-center">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs mb-3" role="tablist">
                            <li class="nav-item col-md-12 col-12 p-0 tab-nav">
                                <a class="nav-link active text-dark font-montserrat font-weight-bold nav-custom-box " data-toggle="tab" href="#regular" role="tab">
                                    <div class="d-none d-md-block">
                                        AR KIDS COLORING
                                    </div>
                                    <div class="d-block d-md-none">
                                        AR KIDS
                                        COLORING
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4 col-2"></div>
            </div>
        </div>
    </section>

    <section class="">
        <div class="tab-content">
            <div class="tab-pane fade show active" id="regular" role="tabpanel">
                <div class="container-fluid">
                    <div class="row no-gutters">
                        <div class="col-12 w-100 bg-regular">
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade show" id="visualdisplay" role="tabpanel">
                <div class="container-fluid">
                    <div class="row no-gutters">
                        <div class="col-12 w-100 bg-vip">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css" type="text/css" media="screen" />


    <style type="text/css">

        .bg-regular{
            background-image: url('{{ asset('images/bsdcustomer/digital/manual - kids coloring.jpg') }}');
            background-repeat: no-repeat;
            background-position: top;
            background-size: contain;
            height:679px;">
        }

        .bg-vip{
            background-image: url('{{ asset('images/bsdcustomer/home/customer journey - vip.jpg') }}');
            background-repeat: no-repeat;
            background-position: center;
            background-size: contain;
            height:830px;">
        }

        .container-fluid{
            padding:0px;
        }
        .header-img{
            height: 190px;
        }

        .box-portofolio{
            margin: 0 auto;
            width: 290px;
            height: 360px;
        }

        .image-portofolio{
            height: 300px;
            background-repeat: no-repeat;
            background-position: center;
            background-size: cover;
            border-radius: 7px;
        }

        .spaces-portfolio-responsive {
            padding-top: 1em;
        }

        .tab-content>.tab-pane,

        .pill-content>.pill-pane {

            display: block;
            /* undo display:none          */

            height: 0;
            /* height:0 is also invisible */

            overflow-y: hidden;
            /* no-overflow                */

        }

        .tab-content>.active,

        .pill-content>.active {

            height: auto;
            /* let the content decide it  */

            overflow-y: visible;

        }

        .header-portfolio-text{
            font-size: 20px;
        }

        .nav-tabs{
            border: none !important;
        }

        .tab-nav {
            border: none !important;
        }
/*
        .nav-link.active.nav-custom-box{
            border-radius: 25px !important;
            background-color: #1d2754;
            margin: 5px;
            color: #fff !important;
            padding: 12px 0;
            font-size: 11px;
        }

        .nav-custom-box{
            border-radius: 25px !important;
            background-color: #f6f6f7;
            margin: 5px;
            color: #1d2754 !important;
            padding: 12px 0;
            font-size: 11px;
            border: 1px solid darkgrey !important;
        }

        .nav-link.nav-custom-box:hover{
            background-color: #1d2754;
            color: #fff !important;
        }*/

        @media (min-width: 350px) {
            .box-portofolio{
                width: 345px;
                height: 430px;
            }

            .image-portofolio{
                height: 370px;
                background-repeat: no-repeat;
                background-position: center;
                background-size: cover;
                border-radius: 7px;
            }
        }

        @media (min-width: 400px) {
            .box-portofolio{
                width: 385px;
                height: 430px;
            }

            .image-portofolio{
                height: 370px;
                background-repeat: no-repeat;
                background-position: center;
                background-size: cover;
                border-radius: 7px;
            }
        }

        @media (min-width: 768px) {
            .bg-vip{
                height:2800px;
            }
            .bg-regular{
                height: 1800px;
            }
            .header-img{
                height: 500px;
            }
/*
            .nav-link.active.nav-custom-box{
                margin: 35px;
                padding: 20px 0;
                font-size: 16px;
            }

            .nav-custom-box{
                margin: 22px;
                padding: 20px 0;
                font-size: 16px;
                border: 1px solid darkgrey !important;
            }*/

            .box-portofolio{
                width: 370px;
                height: 430px;
            }

            .image-portofolio{
                height: 370px;
                background-repeat: no-repeat;
                background-position: center;
                background-size: cover;
                border-radius: 7px;
            }

            .header-portfolio-text{
                font-size: 35px;
            }
        }

        @media (min-width: 992px) {}

        @media (min-width: 1024px) {

            .left-menu {
                border-right: 1px solid #333;
                border-top: 1px solid #333;
            }

            .middle-menu {
                border-top: 1px solid #333;
                border-bottom: 1px solid #333;
            }

            .right-menu {
                border-left: 1px solid #333;
                border-top: 1px solid #333;
                border-bottom: 1px solid #333;
            }

            .tab-left {
                border-top: 1px solid #333;
                border-bottom: 1px solid #333;
                border-right: 1px solid #333;
            }

            .tab-right {
                border-top: 1px solid #333;
                border-bottom: 1px solid #333;
            }

            .four-box {
                height: 100px;
                text-align: center;
                vertical-align: middle;
                line-height: 90px;
            }

            /*.tab-nav {*/
            /*    border-top: 1px solid #333;*/
            /*    border-left: 1px solid #333;*/
            /*    border-bottom: 1px solid #333;*/
            /*}*/

            /*.tab-nav:last-child{*/
            /*    border-right: 1px solid #333;*/
            /*}*/
        }

        @media (min-width: 1200px) {
            .left-menu {
                border-left: 1px solid #333;
                border-right: 1px solid #333;
                border-top: 1px solid #333;
            }

            .right-menu {
                border-right: 1px solid #333;
                border-top: 1px solid #333;
                border-bottom: 1px solid #333;
            }

            .four-box {
                text-align: center;
                vertical-align: middle;
                line-height: 90px;
            }
        }
    </style>
@endsection

@section('scripts')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.min.js"></script>

    <script>


    </script>
@endsection

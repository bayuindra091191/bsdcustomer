@extends('layouts.frontend')

@section('head_and_title')
    <meta name="description" content="BSD Project Description">
    <meta name="author" content="PT. Generasi Muda Gigih">
    <meta name="keywords"
          content="BSD Project Description, Interior, Office, Residential, Apartment, Commercial, Spaces">

    <title>PROJECT DESCRIPTION</title>
@endsection

@section('content')

    <section class="bg-white pt-5 section-body">
        <div class="container-no-padding no-gutters">
            <!-- Title section -->
            <div class="row nav-tab">
                {{--                <div class="col-md-3 col-1"></div>--}}
                <div class="text-center swiper-container">
                    <ul class="nav nav-tabs swiper-wrapper" role="tablist">
                        {{-- Apartment --}}
                        <li class="nav-item tab-nav swiper-slide">
                            <a class="nav-link active text-dark font-montserrat font-weight-bold nav-custom-box"
                               data-toggle="tab" href="#apartment" role="tab">
                                <div class="d-none d-md-block font-tab">
                                    APARTMENT
                                </div>
                                <div class="d-block d-md-none">
                                    APARTMENT
                                </div>
                            </a>
                        </li>
                        <li class="nav-item tab-nav swiper-slide">
                            <a class="nav-link text-dark font-montserrat font-weight-bold nav-custom-box "
                               data-toggle="tab" href="#business" role="tab">
                                <div class="d-none d-md-block font-tab">
                                    BUSINESS LOFT
                                </div>
                                <div class="d-block d-md-none">
                                    BUSINESS LOFT
                                </div>
                            </a>
                        </li>
                        <li class="nav-item tab-nav swiper-slide">
                            <a class="nav-link text-dark font-montserrat font-weight-bold nav-custom-box "
                               data-toggle="tab" href="#industrial" role="tab">
                                <div class="d-none d-md-block font-tab">
                                    INDUSTRIAL LOT
                                </div>
                                <div class="d-block d-md-none">
                                    INDUSTRIAL LOT
                                </div>
                            </a>
                        </li>
                        <li class="nav-item tab-nav swiper-slide">
                            <a class="nav-link text-dark font-montserrat font-weight-bold nav-custom-box "
                               data-toggle="tab" href="#landed" role="tab">
                                <div class="d-none d-md-block font-tab">
                                    LANDED HOUSE
                                </div>
                                <div class="d-block d-md-none">
                                    LANDED HOUSE
                                </div>
                            </a>
                        </li>
                        <li class="nav-item tab-nav swiper-slide">
                            <a class="nav-link text-dark font-montserrat font-weight-bold nav-custom-box "
                               data-toggle="tab" href="#office" role="tab">
                                <div class="d-none d-md-block font-tab">
                                    OFFICE
                                </div>
                                <div class="d-block d-md-none">
                                    OFFICE
                                </div>
                            </a>
                        </li>
                        <li class="nav-item tab-nav swiper-slide">
                            <a class="nav-link text-dark font-montserrat font-weight-bold nav-custom-box "
                               data-toggle="tab" href="#shophouse" role="tab">
                                <div class="d-none d-md-block font-tab">
                                    SHOPHOUSE
                                </div>
                                <div class="d-block d-md-none">
                                    SHOPHOUSE
                                </div>
                            </a>
                        </li>
                        <li class="nav-item tab-nav swiper-slide">
                            <a class="nav-link text-dark font-montserrat font-weight-bold nav-custom-box "
                               data-toggle="tab" href="#soho" role="tab">
                                <div class="d-none d-md-block font-tab">
                                    SOHO
                                </div>
                                <div class="d-block d-md-none">
                                    SOHO
                                </div>
                            </a>
                        </li>
                        <li class="nav-item tab-nav swiper-slide">
                            <a class="nav-link text-dark font-montserrat font-weight-bold nav-custom-box "
                               data-toggle="tab" href="#other" role="tab">
                                <div class="d-none d-md-block font-tab">
                                    OTHER
                                </div>
                                <div class="d-block d-md-none">
                                    OTHER
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
                {{--                    <!-- Tab02 -->--}}
                {{--                    <div class="text-center">--}}
                {{--                        <!-- Nav tabs -->--}}
                {{--                        <ul class="nav nav-tabs mb-3" role="tablist">--}}
                {{--                            <li class="nav-item col-md-12 col-12 p-0 tab-nav">--}}
                {{--                                <a class="nav-link active text-dark font-montserrat-medium nav-custom-box " data-toggle="tab" href="#regular" role="tab">--}}
                {{--                                    <div class="d-none d-md-block">--}}
                {{--                                        REGULAR CUSTOMER--}}
                {{--                                    </div>--}}
                {{--                                    <div class="d-block d-md-none">--}}
                {{--                                        REGULAR<br/>--}}
                {{--                                        CUSTOMER--}}
                {{--                                    </div>--}}
                {{--                                </a>--}}
                {{--                            </li>--}}
                {{--                            <li class="nav-item col-md-12 col-12 p-0 tab-nav">--}}
                {{--                                <a class="nav-link text-dark font-montserrat nav-custom-box font-weight-bold" data-toggle="tab" href="#visualdisplay" role="tab">--}}
                {{--                                    <div class="d-none d-md-block">--}}
                {{--                                        VVIP CUSTOMER--}}
                {{--                                    </div>--}}
                {{--                                    <div class="d-block d-md-none">--}}
                {{--                                        VVIP<br/>--}}
                {{--                                        CUSTOMER--}}
                {{--                                    </div>--}}
                {{--                                </a>--}}
                {{--                            </li>--}}
                {{--                        </ul>--}}
                {{--                    </div>--}}
            </div>
            {{--                <div class="col-md-3 col-1"></div>--}}
        </div>
    </section>

    <section class="bg-white">
        <div class="tab-content pb-5">
            <div class="tab-pane fade show active" id="apartment" role="tabpanel">
                <div class="container-fluid">
                    <div class="row no-gutters">
                        <div class="col-12 w-100 bg-apartment">
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade show" id="business" role="tabpanel">
                <div class="container-fluid">
                    <div class="row no-gutters">
                        <div class="col-12 w-100 bg-business">
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade show" id="industrial" role="tabpanel">
                <div class="container-fluid">
                    <div class="row no-gutters">
                        <div class="col-12 w-100 bg-industrial">
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade show" id="landed" role="tabpanel">
                <div class="container-fluid">
                    <div class="row no-gutters">
                        <div class="col-12 w-100 bg-landed">
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade show" id="office" role="tabpanel">
                <div class="container-fluid">
                    <div class="row no-gutters">
                        <div class="col-12 w-100 bg-office">
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade show" id="shophouse" role="tabpanel">
                <div class="container-fluid">
                    <div class="row no-gutters">
                        <div class="col-12 w-100 bg-shophouse">
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade show" id="soho" role="tabpanel">
                <div class="container-fluid">
                    <div class="row no-gutters">
                        <div class="col-12 w-100 bg-soho">
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade show" id="other" role="tabpanel">
                <div class="container-fluid">
                    <div class="row no-gutters">
                        <div class="col-12 w-100 bg-other">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css"
          type="text/css" media="screen"/>
    <link rel="stylesheet" href="https://unpkg.com/swiper/css/swiper.css">



    <style type="text/css">

        .bg-apartment {
            background-image: url('{{ asset('images/bsdcustomer/project/1 apartment.jpg') }}');
            background-repeat: no-repeat;
            background-position: top center;
            background-size: contain;
            height: 865px;
        ">
        }

        .bg-business {
            background-image: url('{{ asset('images/bsdcustomer/project/2 business loft.jpg') }}');
            background-repeat: no-repeat;
            background-position: top center;
            background-size: contain;
            height: 555px;
        ">
        }

        .bg-industrial {
            background-image: url('{{ asset('images/bsdcustomer/project/3 industrial lot.jpg') }}');
            background-repeat: no-repeat;
            background-position: top center;
            background-size: contain;
            height: 485px;
        ">
        }

        .bg-landed {
            background-image: url('{{ asset('images/bsdcustomer/project/4 landed house.jpg') }}');
            background-repeat: no-repeat;
            background-position: top center;
            background-size: contain;
            height: 2565px;
        ">
        }

        .bg-office {
            background-image: url('{{ asset('images/bsdcustomer/project/5 office.jpg') }}');
            background-repeat: no-repeat;
            background-position: top center;
            background-size: contain;
            height: 1050px;
        ">
        }

        .bg-shophouse {
            background-image: url('{{ asset('images/bsdcustomer/project/6 shophouse.jpg') }}');
            background-repeat: no-repeat;
            background-position: top center;
            background-size: contain;
            height: 1000px;
        ">
        }

        .bg-soho {
            background-image: url('{{ asset('images/bsdcustomer/project/7 soho.jpg') }}');
            background-repeat: no-repeat;
            background-position: top center;
            background-size: contain;
            height: 485px;
        ">
        }

        .bg-other {
            background-image: url('{{ asset('images/bsdcustomer/project/8 other.jpg') }}');
            background-repeat: no-repeat;
            background-position: top center;
            background-size: contain;
            height: 485px;
        ">
        }

        .container-fluid {
            padding: 0px;
        }

        .header-img {
            height: 190px;
        }

        .box-portofolio {
            margin: 0 auto;
            width: 290px;
            height: 360px;
        }

        .image-portofolio {
            height: 300px;
            background-repeat: no-repeat;
            background-position: center;
            background-size: cover;
            border-radius: 7px;
        }

        .spaces-portfolio-responsive {
            padding-top: 1em;
        }

        .tab-content > .tab-pane,
        .pill-content > .pill-pane {

            display: block;
            /* undo display:none          */

            height: 0;
            /* height:0 is also invisible */

            overflow-y: hidden;
            /* no-overflow                */

        }

        .tab-content > .active,
        .pill-content > .active {

            height: auto;
            /* let the content decide it  */

            overflow-y: visible;

        }

        .header-portfolio-text {
            font-size: 20px;
        }

        .nav-tabs {
            border: none !important;
        }

        .tab-nav {
            border: none !important;
        }

        /*.nav-link {
            padding-left: 8px;
            padding-right: 8px;
        }*/

        @media (min-width: 350px) {
            .box-portofolio {
                width: 345px;
                height: 430px;
            }

            .image-portofolio {
                height: 370px;
                background-repeat: no-repeat;
                background-position: center;
                background-size: cover;
                border-radius: 7px;
            }
        }

        @media (min-width: 400px) {
            .box-portofolio {
                width: 385px;
                height: 430px;
            }

            .image-portofolio {
                height: 370px;
                background-repeat: no-repeat;
                background-position: center;
                background-size: cover;
                border-radius: 7px;
            }
        }

        @media (min-width: 768px) {
            .font-tab {
                font-size: 18px;
            }

            .bg-apartment {
                height: 2915px;
            ">
            }

            .bg-business {
                height: 1860px;
            ">
            }

            .bg-industrial {
                height: 1635px;
            ">
            }

            .bg-landed {
                height: 8620px;
            ">
            }

            .bg-office {
                height: 3525px;
            ">
            }

            .bg-shophouse {
                height: 3360px;
            ">
            }

            .bg-soho {
                height: 1630px;
            ">
            }

            .bg-other {
                height: 1630px;
            ">
            }

            .bg-vip {
                height: 2800px;
            }

            .bg-regular {
                height: 3700px;
            }

            .header-img {
                height: 500px;
            }

            .box-portofolio {
                width: 370px;
                height: 430px;
            }

            .image-portofolio {
                height: 370px;
                background-repeat: no-repeat;
                background-position: center;
                background-size: cover;
                border-radius: 7px;
            }

            .header-portfolio-text {
                font-size: 35px;
            }
        }

        @media (min-width: 992px) {
        }

        @media (min-width: 1024px) {

            .left-menu {
                border-right: 1px solid #333;
                border-top: 1px solid #333;
            }

            .middle-menu {
                border-top: 1px solid #333;
                border-bottom: 1px solid #333;
            }

            .right-menu {
                border-left: 1px solid #333;
                border-top: 1px solid #333;
                border-bottom: 1px solid #333;
            }

            .tab-left {
                border-top: 1px solid #333;
                border-bottom: 1px solid #333;
                border-right: 1px solid #333;
            }

            .tab-right {
                border-top: 1px solid #333;
                border-bottom: 1px solid #333;
            }

            .four-box {
                height: 100px;
                text-align: center;
                vertical-align: middle;
                line-height: 90px;
            }

            /*.tab-nav {*/
            /*    border-top: 1px solid #333;*/
            /*    border-left: 1px solid #333;*/
            /*    border-bottom: 1px solid #333;*/
            /*}*/
            /*.tab-nav:last-child{*/
            /*    border-right: 1px solid #333;*/
            /*}*/
        }

        @media (min-width: 1200px) {
            .left-menu {
                border-left: 1px solid #333;
                border-right: 1px solid #333;
                border-top: 1px solid #333;
            }

            .right-menu {
                border-right: 1px solid #333;
                border-top: 1px solid #333;
                border-bottom: 1px solid #333;
            }

            .four-box {
                text-align: center;
                vertical-align: middle;
                line-height: 90px;
            }
        }

        @media (min-width: 1900px) {
            .font-tab {
                font-size: 32px;
            }

            .bg-apartment {
                height: 4435px;
            ">
            }

            .bg-business {
                height: 2820px;
            ">
            }

            .bg-industrial {
                height: 2480px;
            ">
            }

            .bg-landed {
                height: 13100px;
            ">
            }

            .bg-office {
                height: 5355px;
            ">
            }

            .bg-shophouse {
                height: 5110px;
            ">
            }

            .bg-soho {
                height: 2475px;
            ">
            }

            .bg-other {
                height: 2475px;
            ">
            }
        }
    </style>
@endsection

@section('scripts')
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.min.js"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>

    <script src="https://unpkg.com/swiper/js/swiper.js"></script>

    <script>
        $(".slider-project").slick({
            dots: false,
            infinite: false,
            slidesToShow: 3,
            slidesToScroll: 1,
            arrows: false,
        });

    </script>

    <script>
        var swiper = new Swiper('.swiper-container', {
            slidesPerView: 'auto',
            spaceBetween: 20,
            freeMode: true,
            /*pagination: {
                el: '.swiper-pagination',
                clickable: true,
            },*/
        });
    </script>
@endsection

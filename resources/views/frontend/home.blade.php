@extends('layouts.frontend')

@section('head_and_title')
    <meta name="description" content="BSD Customer Journey">
    <meta name="author" content="PT. Generasi Muda Gigih">
    <meta name="keywords" content="BSD Customer Journey, Interior, Office, Residential, Apartment, Commercial, Spaces">

    <title>BSD CUSTOMER JOURNEY</title>
@endsection

@section('content')

    <section class="bg-grey-c mt-5 section-body" style="
    margin-top: 0 !important;">
        <div class="container">
            <!-- Title section -->
            <div class="row d-flex justify-content-center text-center">
            {{--                    col ini grid system dari bootstrap, buat penempatan konten/item, lebar layar dari kiri ke kanan = 12 col. lebih gampang ngerti kalo liat tutorialnya--}}
            <!-- Tab02 -->
            {{--                    tab & nav sih lebih baik coba2 sendiri, aturan & cara pakenya ada tutorial bootstrap ato w3school, sisanya tinggal utak atik cssnya--}}
            <!-- Nav tabs -->
                <ul class="nav nav-tabs mb-3" role="tablist">
                    <li class="nav-item tab-nav">
                        <a class="nav-link active text-dark font-montserrat font-weight-bold nav-custom-box "
                           data-toggle="tab" href="#regular" role="tab">
                            <div class="d-none d-md-block">
                                {{--                                        d-none = display none (ngga kedisplay), d-md-block = baru kedisplay di resolusi md keatas(min-width:576px kalo ga sala) --}}
                                REGULAR CUSTOMER
                            </div>
                            <div class="d-block d-md-none">
                                {{--                                        ya ini kebalikannya--}}
                                REGULAR<br/>
                                CUSTOMER
                            </div>
                        </a>
                    </li>
                    <li class="nav-item tab-nav">
                        <a class="nav-link text-dark font-montserrat font-weight-bold nav-custom-box " data-toggle="tab"
                           href="#visualdisplay" role="tab">
                            <div class="d-none d-md-block">
                                VVIP CUSTOMER
                            </div>
                            <div class="d-block d-md-none">
                                VVIP<br/>
                                CUSTOMER
                            </div>
                        </a>
                    </li>
                </ul>
                {{--<div class="col-md-3 col-1"></div>--}}
            </div>
        </div>
    </section>

    <section class="bg-grey-c">
        <div class="tab-content pb-5">
            <div class="tab-pane fade show active" id="regular" role="tabpanel">
                <div class="container-fluid">
                    <div class="row no-gutters">
                        <div class="col-12 w-100 bg-regular">
                            {{--                            sebenernya cuma biar gampang aja setting responsifnya, jadi imagenya disimpen di dalem class--}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade show" id="visualdisplay" role="tabpanel">
                <div class="container-fluid">
                    <div class="row no-gutters">
                        <div class="col-12 w-100 bg-vip">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    {{--    ini footernya di hide, karna di designnya ga ada footer.--}}
    {{--    kalo mau coba munculin, ada di folder resources->views->frontend->layouts->frontend.blade.php--}}
    {{--    lalu tinggal cari & uncomment @include('partials.frontend._footer')--}}

@endsection

@section('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css"
          type="text/css" media="screen"/>


    <style type="text/css">
        /*banyak class css yg ga kepake & blom diberesin, sisa dari project sebelumnya*/
        .bg-regular {
            background-image: url('{{ asset('images/bsdcustomer/home/customer journey - regular.jpg') }}');
            /*wajib rename image filename ke lowercase, soalnya di mac/apple sering ga ke load imagenya kalo filenamenya ada uppercase*/
            background-repeat: no-repeat;
            background-position: top;
            background-size: contain;
            height: 2000px;
        ">
        }

        .bg-vip {
            background-image: url('{{ asset('images/bsdcustomer/home/customer journey - vip.jpg') }}');
            background-repeat: no-repeat;
            background-position: top;
            background-size: contain;
            height: 1630px;
        ">
        }

        .container-fluid {
            padding: 0px;
        }

        .header-img {
            height: 190px;
        }

        .box-portofolio {
            margin: 0 auto;
            width: 290px;
            height: 360px;
        }

        .image-portofolio {
            height: 300px;
            background-repeat: no-repeat;
            background-position: center;
            background-size: cover;
            border-radius: 7px;
        }

        .spaces-portfolio-responsive {
            padding-top: 1em;
        }

        .tab-content > .tab-pane,
        .pill-content > .pill-pane {

            display: block;
            /* undo display:none          */

            height: 0;
            /* height:0 is also invisible */

            overflow-y: hidden;
            /* no-overflow                */

        }

        .tab-content > .active,
        .pill-content > .active {

            height: auto;
            /* let the content decide it  */

            overflow-y: visible;

        }

        .header-portfolio-text {
            font-size: 20px;
        }

        .nav-tabs {
            border: none !important;
        }

        .tab-nav {
            border: none !important;
        }

        /*.nav-link.active.nav-custom-box{
            border-radius: 25px !important;
            background-color: #1d2754;
            margin: 5px;
            color: #fff !important;
            padding: 12px 0;
            font-size: 11px;
        }

        .nav-custom-box{
            border-radius: 25px !important;
            background-color: #f6f6f7;
            margin: 5px;
            color: #1d2754 !important;
            padding: 12px 0;
            font-size: 11px;
            border: 1px solid darkgrey !important;
        }

        .nav-link.nav-custom-box:hover{
            background-color: #1d2754;
            color: #fff !important;
        }*/

        @media (min-width: 350px) {
            .box-portofolio {
                width: 345px;
                height: 430px;
            }

            .image-portofolio {
                height: 370px;
                background-repeat: no-repeat;
                background-position: center;
                background-size: cover;
                border-radius: 7px;
            }
        }

        @media (min-width: 400px) {
            .box-portofolio {
                width: 385px;
                height: 430px;
            }

            .image-portofolio {
                height: 370px;
                background-repeat: no-repeat;
                background-position: center;
                background-size: cover;
                border-radius: 7px;
            }
        }

        @media (min-width: 768px) {
            .bg-vip {
                height: 2000px;
            }

            .bg-regular {
                height: 2400px;
            }

            .header-img {
                height: 500px;
            }

            /*.nav-link.active.nav-custom-box{
                margin: 22px;
                padding: 20px 0;
                font-size: 16px;
            }

            .nav-custom-box{
                margin: 22px;
                padding: 20px 0;
                font-size: 16px;
                border: 1px solid darkgrey !important;
            }*/
            .box-portofolio {
                width: 370px;
                height: 430px;
            }

            .image-portofolio {
                height: 370px;
                background-repeat: no-repeat;
                background-position: center;
                background-size: cover;
                border-radius: 7px;
            }

            .header-portfolio-text {
                font-size: 35px;
            }
        }

        @media (min-width: 992px) {
        }

        @media (min-width: 1024px) {

            .left-menu {
                border-right: 1px solid #333;
                border-top: 1px solid #333;
            }

            .middle-menu {
                border-top: 1px solid #333;
                border-bottom: 1px solid #333;
            }

            .right-menu {
                border-left: 1px solid #333;
                border-top: 1px solid #333;
                border-bottom: 1px solid #333;
            }

            .tab-left {
                border-top: 1px solid #333;
                border-bottom: 1px solid #333;
                border-right: 1px solid #333;
            }

            .tab-right {
                border-top: 1px solid #333;
                border-bottom: 1px solid #333;
            }

            .four-box {
                height: 100px;
                text-align: center;
                vertical-align: middle;
                line-height: 90px;
            }

            /*.tab-nav {*/
            /*    border-top: 1px solid #333;*/
            /*    border-left: 1px solid #333;*/
            /*    border-bottom: 1px solid #333;*/
            /*}*/
            /*.tab-nav:last-child{*/
            /*    border-right: 1px solid #333;*/
            /*}*/
        }

        @media (min-width: 1200px) {
            .left-menu {
                border-left: 1px solid #333;
                border-right: 1px solid #333;
                border-top: 1px solid #333;
            }

            .right-menu {
                border-right: 1px solid #333;
                border-top: 1px solid #333;
                border-bottom: 1px solid #333;
            }

            .four-box {
                text-align: center;
                vertical-align: middle;
                line-height: 90px;
            }
        }
    </style>
@endsection

@section('scripts')
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.min.js"></script>

    <script>


    </script>
@endsection
